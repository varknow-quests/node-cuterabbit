# In name of Allah
A RESTful node development challenge for pub/sub in a microservice architecture.

## Introduction
What you gotta do is to implement these two services, named Okishiro & Chisashiro. Each service has its own database, apis & resources. Also you will use RabbitMQ to asynchronously transfer data between these two services.

App parts:
### Okishiro Service:
We can add guests to this service, every guest either gets delivered to the Chisashiro service (this is possible only for citizens) or gets feed (meaning the capacity of guest will be fulled with enough food). The citizen guests will get published in the queue with probability of 0.35. Also it is possible to see the list of guests which still are in the castle. **Note** that the guest can eat food only if enough food is available for him/her.

This service has 2 api's:
- add guest: which will get 
```json
Guest: {
	type: GuestType,
    capacity: number
},
GuestType: noble | citizen
    
```
- get guest list: the list of guests which are still in the castle (should be with their ids).

### Chisashiro Service:
We can add foods to this service, and the foods will either be delivered to the Okishiro castle (with probability of 0.6), or they will be eaten by the guests of the castle (if any). 

This service has 2 api's:
- add food: which will get 
```json
Food: {
    name: string,
    count: number
}
```
- get food list: the remaining foods which are still in the castle.

### RabbitMQ
This queue will be implemented in both services, and it should be possible to send foods from Chisashiro to Okishiro, and guests from Okishiro to Chisashiro. 

**Complementary Question**: what should we do if the queue is down?

## Expectations:
We want a clean, readable and maintainable code with meaningful comments and docstrings. Also you need to provide postman API doc for web app. For task managing, you have to break your work to smaller and manageable tasks, and put them on a task managing app, specified by your mentor.

## Tests
It is recommended (not mandatory) to write integration test for this scenario to check your code.

## Task
1. Fork this repository
2. break and specify your tasks in project management tool (trello is advised)
3. Develop the challenge with node.js (you can use express.js)
4. Push your code to your repository
5. Send us a pull request, we will review and get back to you
6. Enjoy 
